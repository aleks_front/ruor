(function () {
    'use strict';
    let servicesItem = document.querySelectorAll('.services__item');

    if ( window.mobileAndTabletcheck() && servicesItem.length || window.innerWidth <= 1024 && servicesItem.length ){
        servicesItem.forEach(function (element) {
            element.addEventListener('click', function (e) {
                const button = this.querySelector('[data-toggle="modal"]');
                button.click();
            });

        });
    }
})();