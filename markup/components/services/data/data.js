var data = {
    services: [
        {
            title: 'Выполнение водолазных работ',
            icon: 'art-diving-work'
        },
        {
            title: 'Промышленный альпинизм',
            icon: 'art-industrial-alpinism'
        },
        {
            title: 'Демонтаж опасно нависающих конструкций',
            icon: 'art-rescuer'
        },
        {
            title: 'Обеспечение спецэффектов и всех видов имитаций',
            icon: 'art-special-effects'
        },
        {
            title: 'Организация салютов',
            icon: 'art-firework'
        },
        {
            title: 'Ремонт и обслуживание гидравлического инструмента, бензоинструмента',
            icon: 'art-hydraulic-equipment'
        },
        {
            title: 'Дежурство спецтранспорта',
            icon: 'art-firefighter'
        },
        {
            title: 'Выставление противопожарных дозоров',
            icon: 'art-fire-patrol'
        },
        {
            title: 'Аренда первичных средств пожаротушения',
            icon: 'art-fire-extinguisher'
        }
    ]
}