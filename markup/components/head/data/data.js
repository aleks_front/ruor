var data = {
    head: {
        defaults: {
            title: 'Заголовок страницы',
            description: 'Описание страницы',
            siteName: 'ruor',
            useSocialMetaTags: false,
            pageName: 'defaults'
        },
        about: {
            title: 'О союзе',
            description: 'Описание страницы',
            siteName: 'ruor',
            useSocialMetaTags: false,
            pageName: 'about'
        },
        activity: {
            title: 'Деятельность',
            description: 'Описание страницы',
            siteName: 'ruor',
            useSocialMetaTags: false,
            pageName: 'activity'
        },
        programms: {
            title: 'Программы',
            description: 'Описание страницы',
            siteName: 'ruor',
            useSocialMetaTags: false,
            pageName: 'programms'
        },
        services: {
            title: 'Услуги',
            description: 'Описание страницы',
            siteName: 'ruor',
            useSocialMetaTags: false,
            pageName: 'services'
        },
        contacts: {
            title: 'Контакты',
            description: 'Описание страницы',
            siteName: 'ruor',
            useSocialMetaTags: false,
            pageName: 'contacts'
        }
    }
}