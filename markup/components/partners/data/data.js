var data = {
    partners: [
        {
            img: 'partners__img-1.png',
            img2x: 'partners__img-1--2x.png',
            title: 'ООДО «За безопасный город»',
            path: 'http://mpk112.ru'
        },
        {
            img: 'partners__img-2.png',
            img2x: 'partners__img-2--2x.png',
            title: 'Общественный совет при ГУ МЧС и Департаменте ГОЧС и ПБ',
            path: 'http://os-mchs.moscow'
        },
        {
            img: 'partners__img-3.png',
            img2x: 'partners__img-3--2x.png',
            title: 'Сервис безопасности РОССОЮЗСПАС',
            path: 'http://saf-service.ru'
        },
        {
            img: 'partners__img-4.png',
            img2x: 'partners__img-4--2x.png',
            title: 'МЧС России',
            path: 'http://www.mchs.gov.ru'
        },
        {
            img: 'partners__img-5.png',
            img2x: 'partners__img-5--2x.png',
            title: 'Всероссийский студенческий корпус спасателей',
            path: 'http://vsks.ru'
        }
    ]
}