(function () {
    'use strict';
    const postRow = document.querySelector('.post__row');
    let postGrid = false;
    let currentPage = 2;

    // init grid
    if (document.readyState === 'complete' || document.readyState !== 'loading') {
        initGrid();
    } else {
        document.addEventListener('DOMContentLoaded', initGrid);
    }

    // destory grid for mobile device
    let resizeTimer;
    window.addEventListener('resize', function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
            initGrid();
        }, 250);
    });
    // add post to grid

    // add more posts
    let itemPagePost = 0;
    document.addEventListener('click', function (e) {
        const elem = e.target;
        const elemToggle = elem.dataset['toggle'];
        const elemTarget = elem.dataset['target'];
        const elemPage = Number(elem.dataset['page']);
        console.log(currentPage);
        if (elemToggle === 'link', elemTarget === 'addMorePost') {
            getPost(function(error, result) {
                if ( error === null && result.length > 0 && postRow) {
                    const fragmentPosts = document.createDocumentFragment();
                    const fragmentGrid = [];
                    for (let i = 0; i < result.length; ++i) {
                        let tmpPost = document.createElement('div');
                        tmpPost.classList.add('post__col', 'row__col');
                        tmpPost.innerHTML += result[i];
                        fragmentPosts.appendChild(tmpPost);
                        fragmentGrid.push(tmpPost);
                    }
                    postRow.appendChild(fragmentPosts);

                    if (postGrid) {
                        postGrid.appended(fragmentGrid);
                    }

                    // set number page for button 
                    pagination(elem);
                    
                    // show or hide button
                    itemPagePost = itemPagePost < result.length ? result.length : itemPagePost;
                    //- console.log(itemPagePost + ' ? ' + result.length);
                    if ( result.length < itemPagePost ) {
                        elem.style.display = 'none';
                    }
                }
            });
        }
    });


    // Init grid width Masonry
    function initGrid() {
        if (window.innerWidth >= 660 && !postGrid && postRow) {
            postGrid = new Masonry('.post__row', {
                itemSelector: '.post__col',
                percentPosition: true,
            });
            imagesLoaded(postRow).on('progress', function () {
                postGrid.layout();
                console.log('layout update');
            });
            let layoutTimer;
            postGrid.on('layoutComplete', function( items ) {
                clearTimeout(layoutTimer);
                resizeTimer = setTimeout(function() {
                    heightGrid();
                }, 250);
            });
        } else if (window.innerWidth < 660 && postGrid) {
            postGrid.destroy();
            postGrid = false;
        }
    }
    // set custom hegith for grid 
    function heightGrid(){
        if (postGrid && postRow) {
            const item = postGrid.items[postGrid.items.length-2];
            const newHeight = item.size.outerHeight + item.position.y;
            postRow.style.height = newHeight + 'px';
            console.log(newHeight);
        }
    }

    // pagination for button
    function pagination(elem) {
        const elemPage = Number(elem.dataset['page']);
        if (elemPage) {
            currentPage = elemPage + 1;
        } else {
            currentPage++;
        }
        elem.setAttribute('data-page', currentPage);
    }

    // get hash nav
    function postNav(){
        const postNav = document.querySelector('#postTabsNav');
        if (postNav){
            const postNavActive = postNav.querySelector('.is-active');
            const postNavUrl = postNavActive.getAttribute('href');
        }
    }
    // postNav();


    // XMLHttpRequest json posts
    if (typeof formatParams === 'undefined') {
        var formatParams = function formatParams(params, method) {
            if (typeof params === 'string') {
                return params;
            }

            var letter = method.toLowerCase() === 'post' ? '' : '?';
            if (Array.isArray(params)) {
                return letter + params.map(function (obj) {
                    return obj.name + "=" + obj.value;
                }).join("&");
            }
            return letter + Object.keys(params).map(function (key) {
                return key + "=" + params[key];
            }).join("&");
        };
    }

    function getPost(newPostsJson) {
        // const url = './uploads/posts/posts.json';
        const url = '/activity/ajax-index';
        const method = 'get';
        let hash_tag = document.getElementById('postTabsNav').getElementsByClassName('nav__link is-active')[0].innerHTML;
		if (hash_tag == 'Показать все') {
            hash_tag = '';
		}

        var data = {
            'page': currentPage,
            'hash_tag': encodeURIComponent(hash_tag)
        };
        let debug = false;
        let callback = '';
        let error = '';

        if (debug) {
            callback('test');
            return;
        }

        const async = false;
        const xhr = new XMLHttpRequest();
        let params = formatParams(data, 'get');
        let body = null;

        if (method.toLowerCase() === 'post') {
            body = formatParams(data, 'post');
            params = '';
        }

        xhr.open(method, url + params, async);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // 'application/json'
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    if (this.responseText.length) {
                        var respText = JSON.parse(this.responseText);
                        newPostsJson(null, respText);
                    } else if (this.responseText.length < 4){
                        newPostsJson(null, null);
                    }
                } else {
                    // error && error(this.responseText);
                    newPostsJson(this.responseText, null);
                }
            }
        };
        xhr.send(body);
    }
}());
