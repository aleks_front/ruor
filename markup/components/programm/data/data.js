var data = {
    programm: {
        gallery: [
            {
                img: 'programm__gallery-img-1.jpg',
                img2x: 'programm__gallery-img-1@2x.jpg',
            },
            {
                img: 'programm__gallery-img-2.jpg',
                img2x: 'programm__gallery-img-2@2x.jpg',
            },
            {
                img: 'programm__gallery-img-3.jpg',
                img2x: 'programm__gallery-img-3@2x.jpg',
            },
            {
                img: 'programm__gallery-img-4.jpg',
                img2x: 'programm__gallery-img-4@2x.jpg',
            },
            {
                img: 'programm__gallery-img-5.jpg',
                img2x: 'programm__gallery-img-5@2x.jpg',
            },
            {
                img: 'programm__gallery-img-6.jpg',
                img2x: 'programm__gallery-img-6@2x.jpg',
            }
        ],
        gallery2: [
            {
                img: 'programm__gallery-img-7.jpg',
                img2x: 'programm__gallery-img-7@2x.jpg',
            },
            {
                img: 'programm__gallery-img-8.jpg',
                img2x: 'programm__gallery-img-8@2x.jpg',
            },
            {
                img: 'programm__gallery-img-9.jpg',
                img2x: 'programm__gallery-img-9@2x.jpg',
            },
            {
                img: 'programm__gallery-img-10.jpg',
                img2x: 'programm__gallery-img-10@2x.jpg',
            },
            {
                img: 'programm__gallery-img-11.jpg',
                img2x: 'programm__gallery-img-11@2x.jpg',
            },
            {
                img: 'programm__gallery-img-12.jpg',
                img2x: 'programm__gallery-img-12@2x.jpg',
            }
        ]
    }
};