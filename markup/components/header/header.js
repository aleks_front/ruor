(function() {
    'use strict';
    document.addEventListener('click', function (e) {
        const elem = e.target, // get target element
            navToggle = elem.dataset['toggle'],
            navTarget = elem.dataset['target'],
            nav = document.querySelector(navTarget);
        if (navToggle === 'nav' && elem.classList.contains('is-active')) {
            closeNav(elem, nav);
        } else if (navToggle === 'nav') {
            openNav(elem, nav);
        }
    });
    
    function openNav(navBtn, nav) {
        navBtn.classList.add('is-active');
        nav.classList.add('is-open');
        slideDown(nav);
    }
    
    function closeNav(navBtn, nav) {
        navBtn.classList.remove('is-active');
        nav.classList.remove('is-open');
        slideUp(nav);
    }
})();