(function () {
    'use strict';
    document.addEventListener('click', function (e) {
        let elem = e.target, // get target element
        customLinkPath = elem.dataset['target'], // get url
        customLinkTarget = elem.dataset['link']; // get link target: _blanks etc
        //console.log(elem);
        if (customLinkTarget && customLinkTarget !== '#') {
            e.preventDefault();
            if ( customLinkPath ) {
                window.open(customLinkPath, customLinkTarget);
            } else {
                window.location.href = customLinkPath;
            }
        }
    });
})();