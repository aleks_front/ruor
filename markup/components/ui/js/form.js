(function () {
    'use strict';

    // custom phone mask
    const inputTel = document.querySelectorAll('[type="tel"]');
    const inputTelMask = ['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

    if (inputTel) {
        let i = 0;
        for (i = inputTel.length - 1; i >= 0; --i) {
            const inputTelMaskInit = vanillaTextMask.maskInput({
                inputElement: inputTel[i],
                mask: inputTelMask,
                showMask: true
            });
        }
    }

    // validation options
    const validOptions = {
        rules: {
            name: {
                required: true,
                minLength: 3,
                maxLength: 15
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                //phone: true,
                minLength: 12,
                maxLength: 19
            }
        },
        messages: {
            name: {
                required: 'Поле обязательно для заполнения',
                maxLength: 'Максимальное количество символов :value characters',
                minLength: 'Минимальное количество символов :value characters'
            },
            phone: {
                required: 'Поле обязательно для заполнения',
                phone: 'Введите коректный номер телефона',
                maxLength: 'Максимальное количество символов :value characters',
                minLength: 'Минимальное количество символов :value characters'
            },
            email: {
                required: 'Поле обязательно для заполнения',
                email: 'Введите коректный электронный адрес'
            }
        },
        submitHandler: function (form, values, ajax) {
            ajax({
                url: '/site/send-application',
                method: 'POST',
                data: values,
                async: true,
                callback: function (response) {
                    modalMessageShow();
                    // console.log(response);
                }
            });
        }
    };
 
    // init validation for form
    const validFormProgramm = document.querySelector('#formProgramm');
    if (validFormProgramm) {
        new window.JustValidate('#formProgramm', validOptions);
    }

    const validFormServices = document.querySelector('#formServices');
    if (validFormServices) {
        new window.JustValidate('#formServices', validOptions);
    }
})();
