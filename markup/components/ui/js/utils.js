// utilits and helpers

// Event listener multiple
function addEventListenerMultiple( element, events, func ) {
    events.split(' ').forEach(e => element.addEventListener(e, func, false));
}

// Offset elements
function getOffset(elem) {
    let el = elem.getBoundingClientRect();
    return {
        top: el.top + pageYOffset,
        left: el.left + pageXOffset,
        width: el.width,
        height: el.height
    };
}