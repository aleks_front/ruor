(function () {
    'use strict';

    document.addEventListener('click', function (e) {
        let elem = e.target, // get target element
            modalToggle = elem.dataset['toggle'],
            modalTarget = elem.dataset['target'],
            formHiddenValue = elem.dataset['value'],
            modalDismis = elem.dataset['dismis'];

        // open modal
        if (modalToggle === 'modal' && modalTarget){
            modalClose ();
            modalShow (modalTarget, formHiddenValue);
        }

        // close modal
        if (modalDismis == 'modal') {
            modalClose();
        }
    });

})();



function modalShow (modalTarget, formHiddenValue) {
    let modal = document.querySelector(modalTarget);
    if (modal){
        overlayOpen ();
        modal.style.display = 'block';
        setTimeout( function () {
            modal.classList.add('is-open');
        }, 250);
        if (formHiddenValue ) {
            let modalFormValue = modal.querySelector('[name="formHiddenValue"]');
            modalFormValue.value = formHiddenValue;
        }
    }
}

function modalClose () {
    let modal = document.querySelector('.modal.is-open');
    if (modal){
        let form = modal.querySelector('form');
        modal.classList.remove('is-open');
        setTimeout( function () {
            modal.style.display = 'none';
            overlayClose();
            modalMessageClose ();
        }, 250);
        if (form){
            form.reset();
        }
    }
}
function modalMessageShow (){
    let modal = document.querySelector('.modal.is-open'),
        modalMessage = modal.querySelector('.modal__message');
    if (modal && modalMessage) {
        modalMessage.style.display = 'flex';
        setTimeout( function () {
            modalMessage.classList.add('is-open');
        }, 250);
    }
}

function modalMessageClose (){
    let modalMessage = document.querySelectorAll('.modal__message');
    if (modalMessage.length > 0) {
        let index, len, item;
        for (index = 0, len = modalMessage.length; index < len; ++index) {
            item = modalMessage[index];
            item.classList.remove('is-open');
            setTimeout( function () {
                item.style.display = 'none';
            }, 250);
        }
    }
}

function overlayOpen () {
    let overlay = document.querySelector('.overlay'),
        page = document.querySelector('.page');

    if (!overlay){
        overlay = document.createElement('div');
        page.appendChild(overlay);
        overlay.outerHTML = '<div class="overlay is-open" data-dismis="modal"></div>';
    }

    overlay.classList.add('is-open');
    page.classList.add('modal-open');
}

function overlayClose (){
    let overlay = document.querySelector('.overlay'),
        page = document.querySelector('.page');

    if (overlay){
        overlay.classList.remove('is-open');
        page.classList.remove('modal-open');
    }
}