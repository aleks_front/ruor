var data = {
    contacts: {
        phone: {
            text: '+7 (495) 947-14-35',
            value: 'tel:+74959471435'
        },
        email: {
            text: 'info@ruor.moscow',
            value: 'mailto:info@ruor.moscow'
        },
        address: {
            text: '123098, РФ, г. Москва, ул. Живописная дом. 28'
        }
    }
}