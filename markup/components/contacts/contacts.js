(function() {
    'use strict';
    var map = document.querySelector('#contactsMap');
    if (map) {
        ymaps.ready(initContactMap);
        let contactsMap, 
            contactsMapPin;

        function initContactMap(){ 
            contactsMap = new ymaps.Map("contactsMap", {
                center: [55.791400, 37.455561],
                zoom: 14,
                controls: []
            });
            suppressMapOpenBlock: false;
            contactsMap.behaviors.disable('scrollZoom');
            
            contactsMapPin = new ymaps.Placemark([55.791400, 37.455561], {
                iconColor: '#ec4638',
                iconCaption: 'Московское отделение Российского Союза Спасателей.',
                hintContent: 'Московское отделение Российского Союза Спасателей.'
            },{
                preset: 'islands#redDotIconWithCaption'
            });
            
            contactsMap.geoObjects.add(contactsMapPin);
        } 
    } 
})();
